//3
db.rooms.insertOne(
	{
		"name": "Single",
		"accomodates": "2",
		"price": 1000,
		"description": "A simple room with all the basic necessities",
		"room_available": 10,
		"isAvailable": false
	}
)

//4
db.rooms.insertMany(
	[
		{
			"name": "Double",
			"accomodates": "3",
			"price": 2000,
			"description": "A room fit for a small family going on vacation.",
			"room_available": 5,
			"isAvailable": false
		},
		{
			"name": "Queen",
			"accomodates": "4",
			"price": 4000,
			"description": "A room with a queen size bed perfect for a simple getaway.",
			"room_available": 15,
			"isAvailable": false
		}
	]
)

//5
db.rooms.find(
	{
		"name":{$eq: "Double"}
	}

)

//6
db.rooms.updateOne(
	{
		"name": "Queen"
	},
	{
		$set:{
			"room_available": 0
		}
	}
)


//7
db.rooms.deleteMany(
	{
		"room_available": {$eq: 0}
	}
)